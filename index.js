// [section] Comments

// comments are parts of the code that gets ignored by the language

// comments are meant to describe the code
//  there are two types of comments:
// -single line comment ctrl + / two slashes(//)

/*

multiline comment
denoted by /**/

/* ctrl + shift /*/

// [SECTION] statements and syntax
	// statements
		// In programming language, statements are instructions that we tell the computer to perform
		// JS statements usually ends with semicolon (;).
			// (;) is also called as delimeter
		// semicolons are not required
			// it use to help us train or locate where a statement ends
	

	// Syntax
		 // In programming, it is the set of rules that describes how statements must be constructed.


console.log("Hello World");

// [SECTION] Variables
 //It is used to contain data. 
// Declaring a variable
	// tells our devices that a variable name is created and is ready to store data.
	//  Syntax: let/const variableName;
	
	// let is a keyword that is usually used in declaring a variable.
	let myVariable;
	// useful for printing values of a variable
	console.log(myVariable);
	// error not hello not define
	/*console.log(hello); */
	// initialize a value / initialization
		// storing the initial value of a variable
		// assignment operator(=)
		// TBLR


	myVariable = "Hello";
		// reassignment
			// changing the initial value to a new value
	myVariable = "Hello World";
	console.log(myVariable);
	// const
		// is used to declare and initialize a constant variable
		// a constant variable does
	// const PI;
	// PI = 3.1416;
	// console.log(PI);

	// declaration with Initialization
		// a variable is given its initial/starting value upon declaration
			// syntax: let/const variableName = value;
			let productName = "Desktop Computer"
			console.log(productName)

			let productPrice = 18999;
			// let productPrice = 20000; error variable has been declared
			console.log(productPrice);

			// const using
			const PI = 3.1416;
			// PI = 3.14; this is an error const does not change
			console.log(PI)

/*
	Guide in writing variables
	1. Use the "let" keyword if the variable will contain different values different values/can change over time.
		Naming Convention: variable name should starts with lowercase characters
		and "camelCasing" is used for multiple words.
	2.Use the const keyword if the variable does not changed
		Naming Convention: "const" keyword should be followed by ALL CAPITAL VARIABLE NAME.(single value variable)

	3.Variable names should be indicative(or descriptive) of the value being stored to avoid confusion.

	Avoid this one: let word = "John Smith"

	4. Variable names are case sentsitive.

*/

// multiple variable declaration

			let productCode = "DC017", productBrand = "Dell";
			console.log(productCode, productBrand);

// using a reserve keyword

			// const let = "hello"; error: lexical bound is dis allowed
			// // console.log(let); 

// [Section] Data Types

// In javaScript, there are six types of data
			// String
				// are series of characters that create a word, a phrase, a sentence or anything related to creating text.
			// Strings in javaScript can be written using a single qoute('') or double ("") 

			let country = "Philippines";
			let	province = 'Metro Manila';

			// concatenating strings
			// multiple string values that can be combined to create a single String using the "+" symbol.
			// i live in manila philippines
			let greeting = "I live in " + province + ", " + country
			console.log(greeting)

			// Escape characters
			// (\) in string combination with other character
			// \n creates a new line
			let mailAddress = "Quezon City\nPhilippines";
			console.log(mailAddress);

			// Expected output: John's employees went home early.
			let message = "John's employees went home early"
			console.log(message)

			message = 'Jane\'s employees went home early.';
			console.log(message)

// numbers
			// Integers/whole numbers
			let headcount = 26;
			console.log(headcount);

			// Decimal Numbers/Fractions
			let grade = 98.7;
			console.log(grade);

			// Exponential Notatio
			let planetDistance = 2e10;
			console.log(planetDistance)

			// combine text and strings
			console.log("John's grade last quarter is " +grade)

			// boolean
				// Boolean values are normally used to store values relating to the state of certain things.
				// True or False(two logical values)

			let isMarried = false;
			let	isGoodConduct = true;

			console.log("isMarried"+isMarried);
			console.log("isMarried"+isGoodConduct);

			// Array
				// are special kind of data type that can be used to store multiple related values.

			// let or cons arrayName = [elementA, elementB, .. elementNTH]
				let grades = [98.7, 92.1, 90.2, 94.6]
				console.log(grades)


				let details = ["John", "Smith", 32, true]
				console.log(details);

				// objects
					// objects are another special kind of data type that is used to mimic real world object/items.
				// used to create complex data that contains information relevant to each other


